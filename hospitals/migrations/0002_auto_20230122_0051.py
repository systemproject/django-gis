# Generated by Django 3.1.7 on 2023-01-22 04:51

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hospitals', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hospital',
            name='beds',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='fid',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='geometry_field',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='latitude',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='longitude',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='province_code',
        ),
        migrations.RemoveField(
            model_name='hospital',
            name='province_name',
        ),
        migrations.AddField(
            model_name='hospital',
            name='addrcity',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='addrfull',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='amenity',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='building',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='capacitype',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='geom',
            field=django.contrib.gis.db.models.fields.MultiPointField(null=True, srid=4326),
        ),
        migrations.AddField(
            model_name='hospital',
            name='healthca_1',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='healthcare',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='operatorty',
            field=models.CharField(max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='osm_id',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='source',
            field=models.CharField(max_length=116, null=True),
        ),
        migrations.AlterField(
            model_name='hospital',
            name='name',
            field=models.CharField(max_length=80, null=True),
        ),
    ]
