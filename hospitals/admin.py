from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from .models import Hospital

class HospitalAdmin(LeafletGeoAdmin):
    list_display = [
        'name', 
        'capacitype', 
        'addrfull', 
        'addrcity', 
        'geom', 
    ]

admin.site.register(Hospital, HospitalAdmin)