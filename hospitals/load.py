from pathlib import Path
from django.contrib.gis.utils import LayerMapping
from .models import Hospital

hospitals_mapping = {
    'osm_id': 'osm_id',
    'source': 'source',
    'capacitype': 'capacitype',
    'operatorty': 'operatorty',
    'healthcare': 'healthcare',
    'building': 'building',
    'healthca_1': 'healthca_1',
    'amenity': 'amenity',
    'addrcity': 'addrcity',
    'addrfull': 'addrfull',
    'name': 'name',
    'geom': 'MULTIPOINT',
}

hospital_shp = Path(__file__).resolve().parent / 'data' / 'hotosm_bra_east_health_facilities_points.shp'

def run(verbose=True):
    layer_mapping = LayerMapping(Hospital, str(hospital_shp), hospitals_mapping, 
		transform=False)
    layer_mapping.save(strict=True, verbose=verbose)