from django.db import models
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _


class Hospital(models.Model):
    osm_id = models.FloatField(null=True)
    source = models.CharField(max_length=116, null=True)
    capacitype = models.CharField(max_length=80, null=True)
    operatorty = models.CharField(max_length=80, null=True)
    healthcare = models.CharField(max_length=80, null=True)
    building = models.CharField(max_length=80, null=True)
    healthca_1 = models.CharField(max_length=80, null=True)
    amenity = models.CharField(max_length=80, null=True)
    addrcity = models.CharField(max_length=80, null=True)
    addrfull = models.CharField(max_length=80, null=True)
    name = models.CharField(max_length=80, null=True)
    geom = models.MultiPointField(srid=4326, null=True)

    def __str__(self):
        return str(self.name)