from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from hospitals import views
from boundaries import views as boundaries_views

router = routers.DefaultRouter()

router.register(
    prefix='api/v1/hospitals', 
    viewset=views.HospitalViewSet,
    basename="hospital"
)

router.register(
    prefix='api/v1/boundaries', 
    viewset=boundaries_views.BoundaryViewSet,
    basename="boundary"
)

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += router.urls

admin.site.site_header = 'Hospitals Admin'
admin.site.site_title = 'GIS Admin Portal'
admin.site.index_title = 'Bem-vindo ao Portal GIS'