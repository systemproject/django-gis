from django.db import models
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

class Boundary(models.Model):
    cd_uf = models.CharField(_("Código UF"),max_length=2, null=True)
    nm_uf = models.CharField(_("Nome UF"),max_length=50, null=True)
    sigla_uf = models.CharField(_("Sigla UF"), max_length=2, null=True)
    nm_regiao = models.CharField(_("Nome Região"), max_length=20, null=True)
    geom = models.MultiPolygonField(_("Geometry Field"),srid=4326, null=True)

    class Meta:
        verbose_name_plural = "Boundaries"