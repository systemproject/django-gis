from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from .models import Boundary

class BoundaryAdmin(LeafletGeoAdmin):
    list_display = [
        'pk', 
        'nm_uf', 
        'sigla_uf'
    ]
    list_display_links = ['nm_uf']

admin.site.register(Boundary, BoundaryAdmin)