from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers
from .models import Boundary

class BoundarySerializer(GeoFeatureModelSerializer):
    area = serializers.CharField(max_length=100)

    class Meta:
        model = Boundary
        geo_field = "geom"
        fields = ["nm_uf","sigla_uf","nm_regiao","area"]