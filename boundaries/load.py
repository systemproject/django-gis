from pathlib import Path
from django.contrib.gis.utils import LayerMapping
from .models import Boundary

boundary_mapping = {
    'cd_uf': 'CD_UF',
    'nm_uf': 'NM_UF',
    'sigla_uf': 'SIGLA_UF',
    'nm_regiao': 'NM_REGIAO',
    'geom': 'MULTIPOLYGON',
}

boundary_shp = Path(__file__).resolve().parent / 'data' / 'BR_UF_2020.shp'

def run(verbose=True):
    layer_mapping = LayerMapping(Boundary, str(boundary_shp), boundary_mapping, 
		transform=False)
    layer_mapping.save(strict=True, verbose=verbose)